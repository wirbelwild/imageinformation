# Changes in Image Information

## 1.6.1 2020-11-24

### Added 

-   Added support for PHP 8.

## 1.6.0 2020-10-30

### Added 

-   Added possibility to read different page boxes from pdf files.