<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Enum;

enum PDFPageBox: string
{
    case ART = 'ArtBox';
    case BLEED = 'BleedBox';
    case CROP = 'CropBox';
    case MEDIA = 'MediaBox';
}
