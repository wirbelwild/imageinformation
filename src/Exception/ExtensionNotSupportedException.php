<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Exception;

use BitAndBlack\ImageInformation\Exception;
use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Class ExtensionNotSupportedException
 *
 * @package BitAndBlack\ImageInformation\Exception
 */
class ExtensionNotSupportedException extends Exception
{
    /**
     * ExtensionNotSupportedException constructor.
     *
     * @param SourceInterface $source
     */
    public function __construct(SourceInterface $source)
    {
        parent::__construct('The extension "' . $source->getExtension() . '" is not supported yet');
    }
}
