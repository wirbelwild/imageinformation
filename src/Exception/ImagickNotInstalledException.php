<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Exception;

use BitAndBlack\ImageInformation\Exception;

/**
 * Class ImagickNotInstalledException
 *
 * @package BitAndBlack\ImageInformation\Exception
 */
class ImagickNotInstalledException extends Exception
{
    /**
     * ImagickNotInstalledException constructor.
     */
    public function __construct()
    {
        parent::__construct('ImageMagick is not available in your system. Be sure to have it installed and add `ext-imagick` to your `composer.json``');
    }
}
