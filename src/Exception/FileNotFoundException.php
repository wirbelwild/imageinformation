<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Exception;

use BitAndBlack\ImageInformation\Exception;

/**
 * Class FileNotFoundException
 * @package BitAndBlack\ImageInformation\Exception
 */
class FileNotFoundException extends Exception
{
    /**
     * FileNotFoundException constructor.
     *
     * @param string $file
     * @param int $code
     * @param \Exception|null $previous
     */
    public function __construct(string $file, int $code = 0, \Exception|null $previous = null)
    {
        parent::__construct(
            'The file "' . $file . '" doesn\'t exist',
            $code,
            $previous
        );
    }
}
