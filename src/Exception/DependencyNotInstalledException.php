<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Exception;

use BitAndBlack\ImageInformation\Exception;

class DependencyNotInstalledException extends Exception
{
    public function __construct(string $dependency)
    {
        parent::__construct('The library "' . $dependency . '" is not available in your system. Add it to your project by running `$ composer require ' . $dependency . '`');
    }
}
