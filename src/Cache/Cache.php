<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Cache;

use BitAndBlack\Composer\Composer;
use BitAndBlack\Duration\Duration;
use BitAndBlack\ImageInformation\Exception\DependencyNotInstalledException;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\SourceInterface;
use Psr\Cache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\CacheInterface as SymfonyCacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

/**
 * Class Cache.
 */
class Cache implements CacheInterface
{
    private readonly SymfonyCacheInterface $cache;

    /**
     * @throws DependencyNotInstalledException
     */
    public function __construct(
        SymfonyCacheInterface|null $cache = null,
        int|null $cacheLifetimeSeconds = null,
        string|null $directory = null,
    ) {
        if (!Composer::classExists(FilesystemAdapter::class)) {
            throw new DependencyNotInstalledException('symfony/cache');
        }

        $this->cache = $cache ?? new FilesystemAdapter(
            'bitandblack-image-information',
            $cacheLifetimeSeconds ?? (int) Duration::createFromDays(7)->getSeconds(),
            $directory
        );
    }

    /**
     * @return array{
     *     size: array{
     *         width: float,
     *         height: float,
     *     },
     * }
     * @throws InvalidArgumentException
     */
    public function getCachedImageInformation(SourceInterface $source): array
    {
        $fileName = urlencode($source->getFile());

        return $this->cache->get(
            $fileName,
            function (ItemInterface $item) use ($source): array {
                $image = new Image($source);

                return [
                    'size' => $image->getSize(),
                ];
            }
        );
    }
}
