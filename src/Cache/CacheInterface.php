<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Cache;

use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Interface CacheInterface.
 */
interface CacheInterface
{
    /**
     * @return array{
     *     size: array{
     *         width: float,
     *         height: float,
     *     },
     * }
     */
    public function getCachedImageInformation(SourceInterface $source): array;
}
