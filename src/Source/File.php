<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Source;

use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\PathInfo\PathInfo;
use Generator;

/**
 * Class Image
 *
 * @package BitAndBlack\ImageInformation
 */
class File implements SourceInterface
{
    private readonly string $file;

    private readonly string $extension;

    /**
     * Image constructor.
     *
     * @throws FileNotFoundException
     */
    public function __construct(string $file)
    {
        if ('' === $file || false === stream_resolve_include_path($file)) {
            throw new FileNotFoundException($file);
        }

        $pathInfo = new PathInfo($file);

        $this->file = $file;
        $this->extension = (string) $pathInfo->getExtension();
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getExtension(): string
    {
        return $this->extension;
    }

    /**
     * @return Generator<string>
     */
    public function read(): Generator
    {
        /** @var resource $file */
        $file = fopen($this->file, 'rb');

        while (false !== ($line = fgets($file))) {
            yield $line;
        }

        fclose($file);
    }
}
