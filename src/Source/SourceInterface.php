<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Source;

use Generator;

/**
 * Interface SourceInterface
 *
 * @package BitAndBlack\ImageInformation\Source
 */
interface SourceInterface
{
    /**
     * Gets the file path
     *
     * @return string
     */
    public function getFile(): string;

    /**
     * Gets the extension
     *
     * @return string
     */
    public function getExtension(): string;

    /**
     * Reads the data
     *
     * @return Generator<string>
     */
    public function read(): Generator;
}
