<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\IccProfile\NullIccProfile;
use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Class IFF
 *
 * @package BitAndBlack\ImageInformation\ImageType
 */
class IFF implements ImageTypeInterface
{
    /**
     * @var array{
     *     width: float,
     *     height: float,
     * }
     */
    private array $size = [
        'width' => 0,
        'height' => 0,
    ];

    /**
     * IFF constructor.
     */
    public function __construct(
        private readonly SourceInterface $source,
    ) {
        $this->read();
    }

    /**
     * Reads the image
     */
    public function read(): void
    {
        $size = getimagesize($this->source->getFile());
        $this->size = [
            'width' => is_array($size) ? (float) $size[0] : 0,
            'height' => is_array($size) ? (float) $size[1] : 0,
        ];
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return $this->size;
    }

    /**
     * @return IccProfileInterface
     */
    public function getIccProfile(): IccProfileInterface
    {
        return new NullIccProfile();
    }
}
