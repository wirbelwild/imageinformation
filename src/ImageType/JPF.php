<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

/**
 * Class JPF
 *
 * @package BitAndBlack\ImageInformation\ImageType
 */
class JPF extends JPG
{
}
