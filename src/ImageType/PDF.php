<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\IccProfile\NullIccProfile;
use BitAndBlack\ImageInformation\Enum\PDFPageBox;
use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Class PDF
 *
 * @package BitAndBlack\ImageInformation\ImageType
 * @see \BitAndBlack\ImageInformation\Tests\ImageType\PDFTest
 */
class PDF implements ImageTypeInterface
{
    /**
     * @var array<string, array<string, float>>
     */
    private array $data;

    /**
     * PDF constructor.
     */
    public function __construct(
        private readonly SourceInterface $source,
    ) {
        $this->read();
    }

    /**
     * Reads the image
     */
    public function read(): void
    {
        $pageBoxesFound = [];
        
        foreach ($this->source->read() as $line) {
            foreach (PDFPageBox::cases() as $pageBox) {
                $box = preg_match(
                    '/' . $pageBox->value . '\[([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,})\]/',
                    $line,
                    $matches
                );

                if ($box) {
                    $pageBoxesFound[$pageBox->value]['width'] = round((float) $matches[3] - (float) $matches[1], 2);
                    $pageBoxesFound[$pageBox->value]['height'] = round((float) $matches[4] - (float) $matches[2], 2);
                }
            }
        }
        
        $this->data = $pageBoxesFound;
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getPageBoxSize(PDFPageBox $pageBox): array
    {
        return [
            'width' => (float) ($this->data[$pageBox->value]['width'] ?? -1),
            'height' => (float) ($this->data[$pageBox->value]['height'] ?? -1),
        ];
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return $this->getPageBoxSize(
            PDFPageBox::ART
        );
    }

    /**
     * @return IccProfileInterface
     */
    public function getIccProfile(): IccProfileInterface
    {
        return new NullIccProfile();
    }
}
