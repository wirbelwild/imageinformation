<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\IccProfile\NullIccProfile;
use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Class AI
 *
 * @package BitAndBlack\ImageInformation\ImageType
 */
class AI implements ImageTypeInterface
{
    /**
     * @var array{
     *     width: float,
     *     height: float,
     * }
     */
    private array $data;

    /**
     * AI constructor.
     */
    public function __construct(
        private readonly SourceInterface $source,
    ) {
        $this->read();
    }

    /**
     * Reads the image
     */
    public function read(): void
    {
        $data = [
            'width' => 0,
            'height' => 0,
        ];

        foreach ($this->source->read() as $line) {
            $bleedBox = preg_match(
                "/BleedBox\[[0-9]{1,}.[0-9]{1,} [0-9]{1,}.[0-9]{1,} ([0-9]{1,}.[0-9]{1,}) ([0-9]{1,}.[0-9]{1,})\]/",
                $line,
                $matches
            );

            if ($bleedBox) {
                $data['width'] = (float) $matches[1];
                $data['height'] = (float) $matches[2];
                break;
            }
        }

        $this->data = $data;
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return [
            'width' => $this->data['width'],
            'height' => $this->data['height'],
        ];
    }

    /**
     * @return IccProfileInterface
     */
    public function getIccProfile(): IccProfileInterface
    {
        return new NullIccProfile();
    }
}
