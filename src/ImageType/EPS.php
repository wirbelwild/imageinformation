<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfile;
use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\IccProfile\NullIccProfile;
use BitAndBlack\ImageInformation\Exception\ImagickNotInstalledException;
use BitAndBlack\ImageInformation\ImageMagickFactory;
use BitAndBlack\ImageInformation\Source\SourceInterface;
use DOMDocument;
use DOMXPath;
use Imagick;
use ImagickException;

/**
 * Class EPS
 *
 * @package BitAndBlack\ImageInformation\ImageType
 */
class EPS implements ImageTypeInterface
{
    private readonly ?Imagick $imagick;

    /**
     * @var array{
     *     width: float,
     *     height: float,
     * }
     */
    private array $size = [
        'width' => 0,
        'height' => 0,
    ];

    /**
     * EPS constructor.
     */
    public function __construct(
        private readonly SourceInterface $source,
    ) {
        $this->imagick = ImageMagickFactory::getImagick($source);
        $this->read();
    }

    /**
     * Reads the image
     */
    public function read(): void
    {
        $dataOriginal = '';
        $dataSorted = [];
        $started = false;
        
        foreach ($this->source->read() as $line) {
            if (str_contains($line, '<x:xmpmeta')) {
                $started = true;
            }
            
            if (!$started) {
                continue;
            }
            
            $dataOriginal .= $line;
            
            if (str_contains($line, '</x:xmpmeta>')) {
                break;
            }
        }

        if (empty($dataOriginal)) {
            return;
        }
        
        $domDocument = new DOMDocument();
        $domDocument->loadXML($dataOriginal);
        
        $xPath = new DOMXPath($domDocument);
        $xPath->registerNamespace('rdf', 'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
        $xPath->registerNamespace('xmpTPg', 'http://ns.adobe.com/xap/1.0/t/pg/');
        
        $query = $xPath->query('/x:xmpmeta/rdf:RDF/rdf:Description/xmpTPg:MaxPageSize');
        
        if (false === $query || 0 === $query->length) {
            return;
        }
        
        if (null === $query->item(0) || !$query->item(0)->hasChildNodes()) {
            return;
        }
        
        foreach ($query->item(0)->childNodes as $item) {
            if ('stDim:w' === $item->nodeName) {
                $dataSorted['width'] = (float) $item->nodeValue;
            } elseif ('stDim:h' === $item->nodeName) {
                $dataSorted['height'] = (float) $item->nodeValue;
            }
        }
        
        $width = $dataSorted['width'] ?? null;
        $height = $dataSorted['height'] ?? null;
        
        if (null !== $width && null !== $height) {
            $this->size = [
                'width' => $width,
                'height' => $height,
            ];
            return;
        }

        if (null === $this->imagick) {
            return;
        }
        
        try {
            $width = (float) $this->imagick->identifyFormat('%w');
            $height = (float) $this->imagick->identifyFormat('%h');
            $this->size = [
                'width' => $width,
                'height' => $height,
            ];
        } catch (ImagickException) {
        }
    }

    /**
     * Returns a relative size!
     *
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return $this->size;
    }

    /**
     * @return IccProfileInterface
     * @throws ImagickNotInstalledException
     */
    public function getIccProfile(): IccProfileInterface
    {
        if (null === $this->imagick) {
            throw new ImagickNotInstalledException();
        }

        try {
            $profiles = $this->imagick->getImageProfiles();
        } catch (ImagickException) {
            $profiles = [];
        }

        if (!isset($profiles['icc'])) {
            return new NullIccProfile();
        }

        return new IccProfile($profiles['icc']);
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     * @throws ImagickNotInstalledException
     */
    public function getBoundingBox(): array
    {
        $boundingBox = [
            'width' => 0,
            'height' => 0,
        ];

        if (null === $this->imagick) {
            throw new ImagickNotInstalledException();
        }
        
        try {
            $boundingBox['width'] = (float) $this->imagick->identifyFormat('%w');
            $boundingBox['width'] = (float) $this->imagick->identifyFormat('%h');
        } catch (ImagickException) {
        }
        
        return $boundingBox;
    }
}
