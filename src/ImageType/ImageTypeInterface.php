<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfileInterface;

/**
 * Interface ImageType.
 */
interface ImageTypeInterface
{
    /**
     * Reads the image.
     *
     * @return void
     */
    public function read(): void;

    /**
     * Returns the width and height of an image.
     *
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array;

    /**
     * Gets the ICC Profile.
     */
    public function getIccProfile(): IccProfileInterface;
}
