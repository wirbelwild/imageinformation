<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\ImageType;

use BitAndBlack\IccProfile\IccProfile;
use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\IccProfile\NullIccProfile;
use BitAndBlack\ImageInformation\Exception\ImagickNotInstalledException;
use BitAndBlack\ImageInformation\ImageMagickFactory;
use BitAndBlack\ImageInformation\Source\SourceInterface;
use Imagick;
use ImagickException;

/**
 * Class PNG
 *
 * @package BitAndBlack\ImageInformation\ImageType
 */
class PNG implements ImageTypeInterface
{
    private readonly ?Imagick $imagick;

    /**
     * @var array{
     *     width: float,
     *     height: float,
     * }
     */
    private array $size = [
        'width' => 0,
        'height' => 0,
    ];
    
    /**
     * PNG constructor.
     */
    public function __construct(
        private readonly SourceInterface $source,
    ) {
        $this->imagick = ImageMagickFactory::getImagick($source);
        $this->read();
    }

    /**
     * Reads the image
     */
    public function read(): void
    {
        $size = getimagesize($this->source->getFile());
        $this->size = [
            'width' => is_array($size) ? (float) $size[0] : 0,
            'height' => is_array($size) ? (float) $size[1] : 0,
        ];
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return $this->size;
    }

    /**
     * @return IccProfileInterface
     * @throws ImagickNotInstalledException
     */
    public function getIccProfile(): IccProfileInterface
    {
        if (null === $this->imagick) {
            throw new ImagickNotInstalledException();
        }

        try {
            $profiles = $this->imagick->getImageProfiles();
        } catch (ImagickException) {
            $profiles = [];
        }

        if (!isset($profiles['icc'])) {
            return new NullIccProfile();
        }

        return new IccProfile($profiles['icc']);
    }
}
