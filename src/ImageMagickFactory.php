<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation;

use BitAndBlack\ImageInformation\Source\SourceInterface;
use Imagick;
use ImagickException;

/**
 * Class ImageMagickFactory.
 *
 * @package BitAndBlack\ImageInformation
 */
class ImageMagickFactory
{
    /**
     * @return Imagick|null
     */
    public static function getImagick(SourceInterface $source): ?Imagick
    {
        if (!extension_loaded('imagick')) {
            return null;
        }

        try {
            return new Imagick($source->getFile());
        } catch (ImagickException) {
            return null;
        }
    }
}
