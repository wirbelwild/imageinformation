<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation;

use BitAndBlack\IccProfile\IccProfileInterface;
use BitAndBlack\ImageInformation\Cache\CacheInterface;
use BitAndBlack\ImageInformation\Exception\ExtensionNotSupportedException;
use BitAndBlack\ImageInformation\ImageType\ImageTypeInterface;
use BitAndBlack\ImageInformation\Source\SourceInterface;

/**
 * Class Reader
 *
 * @package BitAndBlack\ImageInformation
 * @see \BitAndBlack\ImageInformation\Tests\ImageTest
 */
class Image
{
    private ?ImageTypeInterface $imageType = null;

    private readonly string $imageTypeName;

    /**
     * @var array{
     *     width: float,
     *     height: float,
     * }
     */
    private array $size;

    /**
     * @throws ExtensionNotSupportedException
     */
    public function __construct(
        private readonly SourceInterface $source,
        CacheInterface|null $cache = null,
    ) {
        $this->imageTypeName = __NAMESPACE__ . '\\ImageType\\' . strtoupper($source->getExtension());

        if (!class_exists($this->imageTypeName)) {
            throw new ExtensionNotSupportedException($source);
        }

        if (null !== $cache) {
            $cachedImageInformation = $cache->getCachedImageInformation($source);
            $this->size = $cachedImageInformation['size'];
            return;
        }

        $this->size = $this->getImageType()->getSize();
    }

    /**
     * @return array{
     *     width: float,
     *     height: float,
     * }
     */
    public function getSize(): array
    {
        return $this->size;
    }

    /**
     * @throws ExtensionNotSupportedException
     */
    public function getIccProfile(): IccProfileInterface
    {
        return $this->getImageType()->getIccProfile();
    }

    /**
     * Returns the parser itself.
     *
     * @return ImageTypeInterface
     * @throws ExtensionNotSupportedException
     */
    public function getImageType(): ImageTypeInterface
    {
        if (null === $this->imageType) {
            $parser = new $this->imageTypeName($this->source);

            if (!$parser instanceof ImageTypeInterface) {
                throw new ExtensionNotSupportedException($this->source);
            }

            $this->imageType = $parser;
        }

        return $this->imageType;
    }

    /**
     * Returns if the bounding box method can be called.
     *
     * @return bool
     * @throws ExtensionNotSupportedException
     */
    public function hasBoundingBox(): bool
    {
        return method_exists($this->getImageType(), 'getBoundingBox');
    }
}
