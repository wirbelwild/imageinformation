# Upgrades

## 2.x to 3.0

-   The method `getICCProfile` has been renamed: use `getIccProfile` now.