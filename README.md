[![PHP from Packagist](https://img.shields.io/packagist/php-v/bitandblack/image-information)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/bitandblack/image-information/v/stable)](https://packagist.org/packages/bitandblack/image-information)
[![Total Downloads](https://poser.pugx.org/bitandblack/image-information/downloads)](https://packagist.org/packages/bitandblack/image-information)
[![License](https://poser.pugx.org/bitandblack/image-information/license)](https://packagist.org/packages/bitandblack/image-information)

<p align="center">
    <a href="https://www.bitandblack.com" target="_blank">
        <img src="https://www.bitandblack.com/build/images/BitAndBlack-Logo-Full.png" alt="Bit&Black Logo" width="400">
    </a>
</p>

# Bit&Black Image Information 

Read information of images:

*   The size
*   The color profile

Currently supported files and extensions: 

*   `avif` AV1 Image File Format
*   `ai` Adobe Illustrator Artwork
*   `bmp` Windows Bitmap
*   `eps` Encapsulated PostScript 
*   `gif` Graphics Interchange Format 
*   `ico` Initial Coin Offering
*   `iff` Interchange File Format
*   `jp2`, `jpf` (Extended) JPEG 2000
*   `jpg`, `jpeg` Joint Photographic Experts Group
*   `pdf` Portable Document Format
*   `png` Portable Network Graphics 
*   `psd` (Adobe) Photoshop Document
*   `svg` Scalable Vector Graphics
*   `tif`, `tiff` Tagged Image File Format
*   `wbmp` Wireless Application Protocol Bitmap Format
*   `webp` Google WebP

## Installation 

This package is made for the use with [Composer](https://packagist.org/packages/bitandblack/image-information). Add it to your project by running `$ composer require bitandblack/image-information`. 

## Usage

Define your file with the `File` class. Init the `Image` class then and add your file:

````php
<?php

use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\File;

$file = new File('MyFile.eps');
$image = new Image($file);
````

Access the image size: `$image->getSize();`

Access the images color profile: `$image->getIccProfile();`

### Cache 

You can speed up the library by using a cache. When initializing the image class, add a cache object as second parameter:

````php
<?php

use BitAndBlack\ImageInformation\Cache\Cache;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\File;

$file = new File('MyFile.eps');
$image = new Image($file, new Cache());
````

Per default, the cache class uses a file system cache. This can be configured as only a PSR cache compatible class is needed.

## Help 

If you have questions feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).