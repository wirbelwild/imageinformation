<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

use BitAndBlack\ImageInformation\Cache\Cache;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\File;
use BitAndBlack\PathInfo\PathInfo;

require dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

/**
 * This folder contains some images.
 */
$dir = __DIR__ . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

/**
 * We read the whole folder with all files here.
 */
$files = glob($dir . '*');

/**
 * Those are the extensions we want to skip in this example.
 */
$extensionsUnsupported = [
    'idml',
    'indd',
];

/**
 * Set this to `true` if you want to parse the ICC profiles as will.
 * This will read the files from new, as the profile won't get cached yet.
 */
$displayIccProfile = false;

/**
 * Loops every file
 */
foreach ($files as $fileSrc) {
    $pathInfo = new PathInfo($fileSrc);
    $baseName = $pathInfo->getBaseName();
    $extension = strtolower((string) $pathInfo->getExtension());

    if ('image.unsupported' === $baseName
        || in_array($extension, $extensionsUnsupported)
    ) {
        continue;
    }
    
    $time = microtime(true);
    
    $file = new File($fileSrc);
    $image = new Image($file, new Cache());

    $size = $image->getSize();

    echo $fileSrc . ' has a width of ' . $size['width'] . ' and a height of ' . $size['height'] . '. ' . PHP_EOL
        . 'It took ' . round(microtime(true) - $time, 4) . ' seconds to parse.' . PHP_EOL
    ;

    if (true === $displayIccProfile) {
        $time = microtime(true);

        $icc = $image->getIccProfile()->getSpace();

        echo 'ICC profile is ' . ('' !== $icc ? $icc : 'not available') . PHP_EOL
            . 'It took ' . round(microtime(true) - $time, 4) . ' seconds to parse.' . PHP_EOL
        ;
    }
}
