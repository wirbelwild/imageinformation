# Changes in Bit&Black Image Information 3.1

## 3.1.0 2025-01-01

### Changed

-   Add support for [AVIF](https://de.wikipedia.org/wiki/AV1_Image_File_Format) files.