# Changes in Image Information

## 1.5.0 2020-10-01

### Changed 

*   Removed `ext-imagick` and made ImageMagick optional. If a method will be called that needs ImageMagick, the `ImagickNotInstalledException` will be thrown.