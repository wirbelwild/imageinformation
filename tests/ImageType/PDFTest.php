<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Tests\ImageType;

use BitAndBlack\ImageInformation\Enum\PDFPageBox;
use BitAndBlack\ImageInformation\Exception\ExtensionNotSupportedException;
use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\ImageType\PDF;
use BitAndBlack\ImageInformation\Source\File;
use PHPUnit\Framework\TestCase;

/**
 * Class PDFTest.
 *
 * @package BitAndBlack\ImageInformation\Tests\ImageType
 */
class PDFTest extends TestCase
{
    private string $dir;

    /**
     * FileTest constructor.
     */
    protected function setUp(): void
    {
        $this->dir = dirname(__FILE__, 3) . DIRECTORY_SEPARATOR . 'example' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
    }

    /**
     * @throws ExtensionNotSupportedException
     * @throws FileNotFoundException
     */
    public function testCanReadBoxes(): void
    {
        $file = new File($this->dir . 'image.pdf');
        $image = new Image($file);

        /** @var PDF $pdf */
        $pdf = $image->getImageType();
        
        self::assertSame(
            [
                'width' => 456.0,
                'height' => 123.0,
            ],
            $image->getSize()
        );
        
        self::assertSame(
            [
                'width' => 456.0,
                'height' => 123.0,
            ],
            $pdf->getPageBoxSize(
                PDFPageBox::ART
            )
        );

        self::assertSame(
            [
                'width' => 473.01,
                'height' => 140.01,
            ],
            $pdf->getPageBoxSize(
                PDFPageBox::BLEED
            )
        );

        self::assertSame(
            [
                'width' => 514.35,
                'height' => 181.35,
            ],
            $pdf->getPageBoxSize(
                PDFPageBox::CROP
            )
        );
        
        self::assertSame(
            [
                'width' => 514.35,
                'height' => 181.35,
            ],
            $pdf->getPageBoxSize(
                PDFPageBox::MEDIA
            )
        );
    }
}
