<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Tests\ImageType;

use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\ImageInformation\ImageType\JPG;
use BitAndBlack\ImageInformation\Source\File;
use PHPUnit\Framework\TestCase;

/**
 * Class JPGTest.
 *
 * @package BitAndBlack\ImageInformation\Tests\ImageType
 */
class JPGTest extends TestCase
{
    /**
     * @throws FileNotFoundException
     */
    public function testCanReadRGB(): void
    {
        $file = new File(
            dirname(__FILE__, 3) . DIRECTORY_SEPARATOR . 'example' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'image-rgb.jpg'
        );

        $jpg = new JPG($file);

        self::assertSame(
            [
                'width' => 456.0,
                'height' => 123.0,
            ],
            $jpg->getSize()
        );
    }

    /**
     * @throws FileNotFoundException
     */
    public function testCanReadCMYK(): void
    {
        $file = new File(
            dirname(__FILE__, 3) . DIRECTORY_SEPARATOR . 'example' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'image-cmyk.jpg'
        );

        $jpg = new JPG($file);

        self::assertSame(
            [
                'width' => 456.0,
                'height' => 123.0,
            ],
            $jpg->getSize()
        );
    }
}
