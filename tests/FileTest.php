<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Tests;

use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\ImageInformation\Source\File;
use PHPUnit\Framework\TestCase;

/**
 * Class FileTest
 *
 * @package BitAndBlack\ImageInformation\Tests
 */
class FileTest extends TestCase
{
    private string $dir;

    /**
     * FileTest constructor.
     */
    protected function setUp(): void
    {
        $this->dir = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'example' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
    }
    
    public function testThrowsExceptionWhenNotFound(): void
    {
        $this->expectException(FileNotFoundException::class);
        new File('notExisting.tif');
    }
    
    public function testThrowsExceptionWhenEmptyString(): void
    {
        $this->expectException(FileNotFoundException::class);
        new File('');
    }
    
    public function testThrowsExceptionWhenBool(): void
    {
        $this->expectException(FileNotFoundException::class);
        new File('');
    }

    /**
     * Tests the .ai
     *
     * @throws FileNotFoundException
     */
    public function testCanReadAiExtension(): void
    {
        $image = new File($this->dir . 'image.ai');
        self::assertSame('ai', $image->getExtension());
    }

    /**
     * Tests the .bmp
     *
     * @throws FileNotFoundException
     */
    public function testCanReadBmpExtension(): void
    {
        $image = new File($this->dir . 'image.bmp');
        self::assertSame('bmp', $image->getExtension());
    }

    /**
     * Tests the .eps
     *
     * @throws FileNotFoundException
     */
    public function testCanReadEpsExtension(): void
    {
        $image = new File($this->dir . 'image.eps');
        self::assertSame('eps', $image->getExtension());
    }
    
    /**
     * Tests the .gif
     *
     * @throws FileNotFoundException
     */
    public function testCanReadGifExtension(): void
    {
        $image = new File($this->dir . 'image.gif');
        self::assertSame('gif', $image->getExtension());
    }

    /**
     * Tests the .ico
     *
     * @throws FileNotFoundException
     */
    public function testCanReadIcoExtension(): void
    {
        $image = new File($this->dir . 'image.ico');
        self::assertSame('ico', $image->getExtension());
    }

    /**
     * Tests the .iff
     *
     * @throws FileNotFoundException
     */
    public function testCanReadIffExtension(): void
    {
        $image = new File($this->dir . 'image.iff');
        self::assertSame('iff', $image->getExtension());
    }

    /**
     * Tests the .jp2
     *
     * @throws FileNotFoundException
     */
    public function testCanReadJp2Extension(): void
    {
        $image = new File($this->dir . 'image.jp2');
        self::assertSame('jp2', $image->getExtension());
    }

    /**
     * Tests the .jpf
     *
     * @throws FileNotFoundException
     */
    public function testCanReadJpfExtension(): void
    {
        $image = new File($this->dir . 'image.jpf');
        self::assertSame('jpf', $image->getExtension());
    }

    /**
     * Tests the .jpg
     *
     * @throws FileNotFoundException
     */
    public function testCanReadJpgExtension(): void
    {
        $image = new File($this->dir . 'image-rgb.jpg');
        self::assertSame('jpg', $image->getExtension());
    }

    /**
     * Tests the .pdf
     *
     * @throws FileNotFoundException
     */
    public function testCanReadPdfExtension(): void
    {
        $image = new File($this->dir . 'image.pdf');
        self::assertSame('pdf', $image->getExtension());
    }

    /**
     * Tests the .png
     *
     * @throws FileNotFoundException
     */
    public function testCanReadPngExtension(): void
    {
        $image = new File($this->dir . 'image.png');
        self::assertSame('png', $image->getExtension());
    }

    /**
     * Tests the .pdf
     *
     * @throws FileNotFoundException
     */
    public function testCanReadPsdExtension(): void
    {
        $image = new File($this->dir . 'image.psd');
        self::assertSame('psd', $image->getExtension());
    }

    /**
     * Tests the .svg
     *
     * @throws FileNotFoundException
     */
    public function testCanReadSvgExtension(): void
    {
        $image = new File($this->dir . 'image.svg');
        self::assertSame('svg', $image->getExtension());
    }

    /**
     * Tests the .tif
     *
     * @throws FileNotFoundException
     */
    public function testCanReadTifExtension(): void
    {
        $image = new File($this->dir . 'image-rgb.tif');
        self::assertSame('tif', $image->getExtension());
    }

    /**
     * Tests the .wbmp
     *
     * @throws FileNotFoundException
     */
    public function testCanReadWbmpExtension(): void
    {
        $image = new File($this->dir . 'image.wbmp');
        self::assertSame('wbmp', $image->getExtension());
    }

    /**
     * Tests the .webp
     *
     * @throws FileNotFoundException
     */
    public function testCanReadWebpExtension(): void
    {
        $image = new File($this->dir . 'image.webp');
        self::assertSame('webp', $image->getExtension());
    }

    /**
     * Tests the .avif
     *
     * @throws FileNotFoundException
     */
    public function testCanReadAvifExtension(): void
    {
        $image = new File($this->dir . 'image.avif');
        self::assertSame('avif', $image->getExtension());
    }
}
