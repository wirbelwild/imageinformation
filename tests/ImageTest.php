<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\Tests;

use BitAndBlack\ImageInformation\Exception\ExtensionNotSupportedException;
use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\File;
use PHPUnit\Framework\TestCase;
use Psr\Cache\InvalidArgumentException;

/**
 * Class ImageTest
 *
 * @package BitAndBlack\ImageInformation\Tests
 */
class ImageTest extends TestCase
{
    private string $dir;

    /**
     * ImageTest constructor.
     */
    protected function setUp(): void
    {
        $this->dir = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'example' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
    }

    /**
     * @throws ExtensionNotSupportedException
     * @throws FileNotFoundException
     * @throws InvalidArgumentException
     */
    public function testCanReadImageSize(): void
    {
        $unsupportedExtensions = [
            'indd',
            'idml',
        ];
        
        /**
         * We read the whole folder with all files here
         */
        $files = glob($this->dir . '*') ?: [];

        foreach ($files as $fileSrc) {
            $extension = pathinfo($fileSrc)['extension'] ?? null;
            
            if ('unsupported' === $extension || 'imagecache' === $extension || in_array($extension, $unsupportedExtensions, true)) {
                continue;
            }

            $file = new File($fileSrc);
            $image = new Image($file);

            $expectedSize = [
                'width' => 456,
                'height' => 123,
            ];
            
            $actualSize = $image->getSize();
            
            if ('ico' === $extension) {
                $expectedSize = [
                    'width' => 123,
                    'height' => 123,
                ];
            }
            
            self::assertEquals(
                $expectedSize,
                $actualSize,
                $file->getFile()
            );
        }
    }

    /**
     * @throws ExtensionNotSupportedException
     * @throws FileNotFoundException
     * @throws InvalidArgumentException
     */
    public function testThrowsExceptionWhenExtensionNotSupported(): void
    {
        $this->expectException(ExtensionNotSupportedException::class);
        
        $image = new File($this->dir . 'image.unsupported');
        new Image($image);
    }

    /**
     * @throws ExtensionNotSupportedException
     * @throws FileNotFoundException
     * @throws InvalidArgumentException
     */
    public function testCanDifferBoundingBox(): void
    {
        $file = new File($this->dir . 'image.eps');
        $image = new Image($file);

        self::assertTrue(
            $image->hasBoundingBox()
        );

        $boundingBox = $image->getSize();
        
        if (method_exists($image->getImageType(), 'getBoundingBox')) {
            $boundingBox = $image->getImageType()->getBoundingBox();
        }
        
        self::assertNotSame(
            $image->getSize(),
            $boundingBox
        );
    }
}
