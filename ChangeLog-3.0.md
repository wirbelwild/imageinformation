# Changes in Bit&Black Image Information 3.0

## 3.0.0 2024-10-11

### Changed

-   PHP >=8.1 is now required.
-   The method `getICCProfile` has been renamed to `getIccProfile`.