<?php

/**
 * Bit&Black Image Information.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.bitandblack.com
 * @license MIT
 */

namespace BitAndBlack\ImageInformation\DevTools\PHPStan;

use BitAndBlack\ImageInformation\Enum\PDFPageBox;
use PHPStan\Reflection\ConstantReflection;
use PHPStan\Rules\Constants\AlwaysUsedClassConstantsExtension;

class ConstantsExtension implements AlwaysUsedClassConstantsExtension
{
    /**
     * @var array<int, string>
     */
    private array $allowedConstants = [];

    public function __construct()
    {
        array_push($this->allowedConstants, ...PDFPageBox::cases());
    }

    public function isAlwaysUsed(ConstantReflection $constant): bool
    {
        return in_array($constant->getName(), $this->allowedConstants, true);
    }
}
