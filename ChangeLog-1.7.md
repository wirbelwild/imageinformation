# Changes in Bit&Black Image Information 1.7

## 1.7.0 2021-04-22

### Fixed 

-   Added missing handling of CMYK images.

### Changed

-   Removed `bitandblack/helpers` and `marc1706/fast-image-size`.
-   Improved handling of Imagemagick.